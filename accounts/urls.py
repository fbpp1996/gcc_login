from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^accounts/profile/$', views.home),
    url(r'^logout/', views.user_logout),
]